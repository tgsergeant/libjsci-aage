package net.slashie.libjcsi.wswing;

import java.awt.image.BufferedImage;

/**
 * @author Tim Sergeant
 */
public interface ScreenshotListener {

	public void handleScreenshot(BufferedImage screenshot);
}
